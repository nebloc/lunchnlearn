CREATE TABLE IF NOT EXISTS public.submissions
(
    title text NOT NULL,
    "desc" text,
    presenter text NOT NULL,
    length integer,
    id serial NOT NULL,
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.submissions
    OWNER to lunchnlearn;