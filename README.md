# Lunch N Learn

This is a demo project to show what Kubernetes can do and how to do it.
It consists of a Golang API in backend, a PostgreSQL database in database, and a frontend written in Vue.JS with Bulma styling. 

The ingress.yml file is for an NGINX ingress controller and the domain demo.nebloc.com. The namespace already has a Cert-manager running to provide SSL certificates to the incoming traffic from letsencrypt.
