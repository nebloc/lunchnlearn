import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    submissions: [],
    error: null
  },
  mutations: {
    getSubmissions(state, subs) {
      state.submissions = subs
    },
    errorSet(state, err) {
      state.error = err
    }
  },
  actions: {
    getSubmissions(context) {
      axios.get("/api/v1/submissions").then(resp => {
        context.commit('getSubmissions', resp.data)
        context.commit('errorSet', null)
      }).catch(err => {
        context.commit('errorSet', err)
      })
    },
    addSubmission(context, sub) {
      axios.post("/api/v1/submissions", sub).then(resp => {
        context.dispatch("getSubmissions")
      }).catch(err => {
        console.log(err.data)
        context.commit('errorSet', err)
      })
    },
    deleteSubmission(context, id) {
      axios.delete("/api/v1/submissions/" + id).then(_ => {
        context.dispatch('getSubmissions')
      }).catch(err => {
        context.commit('errorSet', err)
      })
    }
  }
})
