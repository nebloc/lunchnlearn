module github.com/nebloc/lunchnlearn/backend

go 1.12

require (
	github.com/gin-gonic/gin v1.4.0
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.1.1
	github.com/sirupsen/logrus v1.4.1
)
