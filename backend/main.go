package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

var db *sqlx.DB

func main() {
	log.SetFormatter(&log.JSONFormatter{})
	connStr := `host=database port=5432 user=lunchnlearn dbname=lunchnlearn sslmode=disable password=HelloTBS1234`
	var err error
	db, err = sqlx.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}
	router := gin.New()
	router.Use(Logging())
	router.NoRoute(func(c *gin.Context) {
		c.String(http.StatusNotFound, "Hello TBS. This is not a path.")
	})
	api := router.Group("/api/v1")
	api.GET("/kill", fallOver)
	api.GET("/submissions", listSubmissions)
	api.POST("/submissions", postSubmissions)
	api.DELETE("/submissions/:id", deleteSubmission)
	router.Run(":8080")
}

func listSubmissions(c *gin.Context) {
	subs := make([]Submission, 0)
	err := db.Select(&subs, `SELECT * FROM submissions`)
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, subs)
}

func postSubmissions(c *gin.Context) {
	var sub Submission
	err := c.BindJSON(&sub)
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}
	_, err = db.NamedExec(`INSERT INTO submissions (title, "desc", presenter, length) 
	VALUES (:title,:desc,:presenter,:length);`, sub)
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	c.Status(http.StatusCreated)
}

func deleteSubmission(c *gin.Context) {
	id := c.Param("id")
	_, err := db.Exec(`DELETE FROM submissions WHERE id=$1`, id)
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	c.Status(http.StatusOK)
}

func fallOver(c *gin.Context) {
	log.Fatal("Dying...")
	c.JSON(http.StatusOK, gin.H{})
}

type Submission struct {
	Title     string `json:"title,omitempty" db:"title"`
	Desc      string `json:"desc,omitempty" db:"desc"`
	Presenter string `json:"presenter,omitempty" db:"presenter"`
	Length    int    `json:"length,omitempty" db:"length"`
	Id        int    `json:"id,omitempty" db:"id"`
}
