package main

import (
	"fmt"
	"time"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
)

// Logging provides a middleware func to log Gin requests with Logrus to use
// the logging format.
func Logging() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Start timer
		start := time.Now()
		path := c.Request.URL.Path
		raw := c.Request.URL.RawQuery

		// Process request
		c.Next()

		// Stop timer
		end := time.Now()
		latency := end.Sub(start)

		clientIP := c.ClientIP()
		method := c.Request.Method
		statusCode := c.Writer.Status()

		comment := c.Errors.ByType(gin.ErrorTypePrivate).String()

		if raw != "" {
			path = path + "?" + raw
		}
		log.WithFields(log.Fields{
			"status":  statusCode,
			"latency": fmt.Sprintf("%v", latency),
			"ip":      clientIP,
			"method":  method,
			"path":    path,
			"comment": comment,
		}).Info(path)
	}
}